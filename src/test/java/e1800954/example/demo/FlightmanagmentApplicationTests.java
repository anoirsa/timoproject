package e1800954.example.demo;

import e1800954.example.demo.controller.FlightController;
import e1800954.example.demo.controller.PassengerController;
import e1800954.example.demo.model.Flight;
import e1800954.example.demo.model.Passenger;
import e1800954.example.demo.model.TravelClass;
import e1800954.example.demo.repository.FlightRepository;
import e1800954.example.demo.repository.PassengerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.*;

@SpringBootTest
class FlightmanagmentApplicationTests {

	@Autowired
	FlightRepository flightRepository;

	@Autowired
	FlightController flightController;

	@Autowired
	PassengerRepository passengerRepository;
	@Autowired
	PassengerController passengerController;

	@Test
	public void TestRepos() {
		Passenger passenger = new Passenger("Johns coe", TravelClass.ECONOMY,
						"02121324","anouarbelis@gmail.com",1L);
		Passenger passenger2 = new Passenger("GOs", TravelClass.ECONOMY,
				"021651324","Moss@gmail.com",1L);
		Passenger passenger3 = new Passenger("Micahel", TravelClass.ECONOMY,
				"021651324","Moss@gmail.com",1L);

		passengerRepository.save(passenger);
		passengerRepository.save(passenger2);

		Passenger found = passengerRepository.findById(1L).orElse(null);
		assertEquals(found.toString(),passenger.toString());
		assertEquals(passengerRepository.findAll().size(),2);
		assertEquals(passengerController.getPassengerById(1L).toString(),passenger.toString());
		passengerController.addNewPassenger(passenger3);

		Flight flight1 = new Flight("Oslo","Helsinki","15-11-2020");
		Flight flight2 = new Flight("Roma","Madrid","20-11-2019");
		Flight flight3 = new Flight("Barcalona","London","12-10-2018");

		flightRepository.save(flight1);
		flightRepository.save(flight2);
		Flight found1 = flightRepository.findById(1L).orElse(null);
		assertEquals(flight1.toString(),flight1.toString());
		assertEquals(2,flightRepository.findAll().size());
		flightController.addNewFlight(flight3);







	}

}
