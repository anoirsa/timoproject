package e1800954.example.demo.controller;


import e1800954.example.demo.model.Passenger;
import e1800954.example.demo.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(allowedHeaders = "*", origins = "*")
@RequestMapping("api/passengers")
public class PassengerController {
    private PassengerService passengerService;

    @Autowired
    public PassengerController(PassengerService passengerService) {
        this.passengerService = passengerService;
    }


    @GetMapping
    public List<Passenger> getAllPassenger() {
        return passengerService.getAllPassengers();
    }

    @PostMapping
    public @ResponseBody
    Passenger addNewPassenger(@RequestBody Passenger passenger) {
        return passengerService.insertNewPassenger(passenger);
    }

    @GetMapping(path ="{id}")
    public @ResponseBody
    Passenger getPassengerById(@PathVariable("id") Long id) {
        return passengerService.getPassengerById(id).orElse(null);
    }

    @DeleteMapping(path = "{id}")
    public @ResponseBody
    String deletePassengeById(@PathVariable("id") Long id) {
        return  passengerService.deleteById(id);
    }

    @PutMapping(
            path = "{whatToChange}/{id}"
    )
    public @ResponseBody
    String changePassengerInfos(@PathVariable("whatToChange") String whatToChange ,
                                @PathVariable("id") Long id,
                                @RequestBody String newValue) {
        return passengerService.updateInfos(whatToChange,id, newValue);
    }


}
