package e1800954.example.demo.controller;

import e1800954.example.demo.model.Flight;
import e1800954.example.demo.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@CrossOrigin(origins = "*" , allowedHeaders = "*")
@RequestMapping("api/flights")
public class FlightController {
    private FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @PostMapping
    public @ResponseBody
    Flight addNewFlight (@RequestBody Flight flight) {
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-mm-yyyy");
        return flightService.addNewFlight(flight);
    }

    @GetMapping
    public  @ResponseBody
    List<Flight> getAllFlights() {
        return flightService.getAllFlights();
    }

    @GetMapping(path = "{id}")
    public @ResponseBody
    Flight findFlightById(@PathVariable("id") Long id) {
        return flightService.findFlightById(id).orElse(null);
    }

    @DeleteMapping(
            path = "{id}"
    )
    public @ResponseBody
    String deleteFlightById(@PathVariable("id") Long id) {
        return flightService.deleteFlightById(id);
    }

    @PutMapping(
            path = "{whatToChange}/{id}"
    )
    public @ResponseBody
    String changeFlightDetails (@PathVariable("whatToChange")String whatToChange,
                                @PathVariable("id") Long id,
                                @RequestBody String newValue) {
       return flightService.updateInfos(whatToChange,id,newValue);
    }





}
