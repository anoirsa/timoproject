package e1800954.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity(
        name = "Passenger"
)

@Table(
        name = "passengers"
)
public class Passenger {
    @Id
    @SequenceGenerator(name ="passenger_sequence",
                       sequenceName = "passenger_sequence",
                        allocationSize = 1)
    @GeneratedValue (
            strategy = GenerationType.SEQUENCE,
            generator = "passenger_sequence"
    )

    @Column (
            name = "passenger_id"
    )
    private Long passengerId;
    @Column(
            name = "passenger_name",
            nullable = false
    )
    private String passengerName;
    @Column(
            name = "travel_class",
            nullable = false
    )
    private TravelClass travelClass;
    @Column(
            name = "phone_number",
            nullable = false
    )
    private String phoneNumber;
    @Column(
            name = "email"
    )
    private String email;

    public Long flightId;


    public Passenger(@JsonProperty("passengerName") String passengerName,
                     @JsonProperty("travelClass")TravelClass travelClass,
                     @JsonProperty("phoneNumber")String phoneNumber,
                     @JsonProperty("email")String email,
                     @JsonProperty("flightId") Long flightId) {
        this.flightId = flightId;
        this.passengerName = passengerName;
        this.travelClass = travelClass;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public  Passenger() {
        
    }

    public Long getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(Long passengerId) {
        this.passengerId = passengerId;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public TravelClass getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(TravelClass travelClass) {
        this.travelClass = travelClass;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "passengerId=" + passengerId +
                ", passengerName='" + passengerName + '\'' +
                ", travelClass=" + travelClass +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", flightId=" + flightId +
                '}';
    }
}
