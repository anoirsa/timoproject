package e1800954.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(
        name = "Flight"
)

@Table(name = "flights")
public class Flight {

    @Id
    @SequenceGenerator(
            name = "flight_sequence",
            sequenceName = "flight_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "flight_sequence"
    )

    @Column(
            name = "flight_id",
            nullable = false
    )
    private Long flightId;
    @Column(
            name = "departure"
    )
    private String departure;
    @Column(
            name = "arrival"
    )
    private String arrival;
    @Column(
            name = "flight_time",
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE"
    )
    private LocalDate flightTime;


    public Flight(@JsonProperty("departure") String departure,
                  @JsonProperty("arrival")String arrival,
                  @JsonProperty("flightTime") String flightTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dati = LocalDate.parse(flightTime , formatter);
        this.departure = departure;
        this.arrival = arrival;
        this.flightTime = dati;
    }

    public Flight(){

    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public String getdeparture() {
        return departure;
    }

    public void setdeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public LocalDate getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(LocalDate flightTime) {
        this.flightTime = flightTime;
    }



    @Override
    public String toString() {
        return "Flight{" +
                "flightId=" + flightId +
                ", departure='" + departure + '\'' +
                ", arrival='" + arrival + '\'' +
                ", flight_time=" + flightTime +
                '}';
    }
}
