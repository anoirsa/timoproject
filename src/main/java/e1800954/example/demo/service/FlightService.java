package e1800954.example.demo.service;

import e1800954.example.demo.model.Flight;
import e1800954.example.demo.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlightService {
    private FlightRepository flightRepository;

    @Autowired
    public FlightService( FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public Flight addNewFlight (Flight flight) {
        return flightRepository.save(flight);
    }

    public List<Flight> getAllFlights () {
        return flightRepository.findAll();
    }
    public Optional<Flight> findFlightById(Long id) {
        return flightRepository.findById(id);
    }

    public String deleteFlightById(Long id) {
        boolean isFound = flightRepository.existsById(id);
        flightRepository.deleteById(id);
        return isFound ? "Delete went successful" :  "ID error";
    }

    public String updateInfos(String whatToUpdate, Long id, String newValue) {
        if (flightRepository.existsById(id)) {
            String result;
            switch (whatToUpdate) {
                case "arrival" :
                    flightRepository.updateArrival(id,newValue);
                    result = "Arrival updated";
                    break;
                case "departure":
                    flightRepository.updateDeparture(id,newValue);
                    result = "Departure Updated";
                    break;
                case  "flight_time" :
                    flightRepository.updateFlightTime(id,newValue);
                    result = "Flight time updated";
                    break;
                default:
                    result = "Argument false";
                    break;
            }
            return result;
        }
        else {
            return "ID not Valid";
        }
    }
}
