package e1800954.example.demo.service;


import e1800954.example.demo.model.Passenger;
import e1800954.example.demo.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static e1800954.example.demo.service.InfoValidationResults.*;
import java.util.List;
import java.util.Optional;

import static e1800954.example.demo.service.PassengerInfoValidator.*;

@Service
public class PassengerService {
    private PassengerRepository passengerRepository;

    @Autowired
    public PassengerService(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    public List<Passenger> getAllPassengers() {
        return passengerRepository.findAll();
    }

    public Passenger insertNewPassenger(Passenger passenger) {
        InfoValidationResults areInfosValid = isEmailValid().and(isPhoneNumberValid()).apply(passenger);
        return  areInfosValid.equals(SUCCESS) ? passengerRepository.save(passenger) : null;
    }

    public Optional<Passenger> getPassengerById(Long id) {
        return passengerRepository.findById(id);
    }

    public String deleteById(Long id) {
        boolean exist = passengerRepository.existsById(id);
        passengerRepository.deleteById(id);
        return exist ? "Deleted Successfuly" : "ID not valid";
    }

    public String updateInfos(String whatToUpdate, Long id, String newValue) {
        if (passengerRepository.existsById(id)) {
            String result;
            switch (whatToUpdate) {
                case "email" :
                    passengerRepository.updateEmail(id,newValue);
                    result = "Email updated";
                    break;
                case "phone_number":
                    passengerRepository.updatePhoneNumber(id,newValue);
                    result = "Phone Number Updated Updated";
                    break;
                case  "passenger_name" :
                    passengerRepository.updateName(id,newValue);
                    result = "Passenger name time updated";
                    break;
                case "class" :
                    passengerRepository.updateFlightClass(id,newValue);
                    result = "The flight class has been updated";
                default:
                    result = "Argument false";
                    break;
            }
            return result;
        }
        else {
            return "ID not Valid";
        }
    }

}
