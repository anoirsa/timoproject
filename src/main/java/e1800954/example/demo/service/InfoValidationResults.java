package e1800954.example.demo.service;

public enum InfoValidationResults {
    SUCCESS,
    PHONE_NUMBER_NOT_VALID,
    EMAIL_NOT_VALID,
}
