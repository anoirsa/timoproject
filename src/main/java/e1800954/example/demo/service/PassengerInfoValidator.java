package e1800954.example.demo.service;
import java.util.function.Function;
import e1800954.example.demo.model.Passenger;

import static e1800954.example.demo.service.InfoValidationResults.*;

public interface PassengerInfoValidator extends Function <Passenger,InfoValidationResults >{
    static  PassengerInfoValidator isEmailValid() {
        return passenger -> passenger.getEmail().contains("@") ? SUCCESS : EMAIL_NOT_VALID;
    }

    static PassengerInfoValidator isPhoneNumberValid() {
        return passenger -> passenger.getPhoneNumber().startsWith("0") ? SUCCESS :
                            PHONE_NUMBER_NOT_VALID;
    }

    default PassengerInfoValidator and (PassengerInfoValidator other) {
        return passenger -> {
            InfoValidationResults result = this.apply(passenger);
            return  result.equals(SUCCESS) ? other.apply(passenger) : result;
        };
    }
}
