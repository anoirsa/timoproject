package e1800954.example.demo;

import e1800954.example.demo.model.Flight;
import e1800954.example.demo.model.Passenger;
import e1800954.example.demo.model.TravelClass;
import e1800954.example.demo.repository.FlightRepository;
import e1800954.example.demo.repository.PassengerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import static e1800954.example.demo.model.TravelClass.ECONOMY;

@SpringBootApplication
public class FlightmanagmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightmanagmentApplication.class, args);
	}


}
