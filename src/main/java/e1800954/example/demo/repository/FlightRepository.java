package e1800954.example.demo.repository;

import e1800954.example.demo.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface FlightRepository extends JpaRepository<Flight, Long> {

    @Modifying
    @Query(value = "UPDATE flights SET arrival=?2 WHERE flight_id = ?1", nativeQuery = true)
    void updateArrival( Long id , String newValue );


    @Modifying
    @Query(value = "UPDATE flights SET departure=?2 WHERE flight_id = ?1", nativeQuery = true)
    void updateDeparture( Long id , String newValue );

    @Modifying
    @Query(value = "UPDATE flights SET flight_time=?2 WHERE flight_id = ?1", nativeQuery = true)
    void updateFlightTime( Long id , String newValue );

}
