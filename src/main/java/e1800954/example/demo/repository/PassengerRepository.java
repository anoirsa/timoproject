package e1800954.example.demo.repository;

import e1800954.example.demo.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface PassengerRepository extends JpaRepository<Passenger , Long> {

    @Modifying
    @Query(value = "UPDATE passengers SET passenger_name=?2 WHERE passenger_id = ?1", nativeQuery = true)
    void updateName( Long id , String newValue );

    @Modifying
    @Query(value = "UPDATE passengers SET email=?2 WHERE passenger_id = ?1", nativeQuery = true)
    void updateEmail( Long id , String newValue );

    @Modifying
    @Query(value = "UPDATE passengers SET phone_number=?2 WHERE passenger_id = ?1", nativeQuery = true)
    void updatePhoneNumber( Long id , String newValue );

    @Modifying
    @Query(value = "UPDATE passengers SET flight_class=?2 WHERE passenger_id = ?1", nativeQuery = true)
    void updateFlightClass( Long id , String newValue );


}
